<?php
namespace Drupal\onsen\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Onsen module.
 */
class Demo extends ControllerBase {

  /**
   * Returns a demo page.
   *
   * @return array
   *   A simple renderable array with a custom var.
   */
  public function demo() {
    
    return [
      '#theme' => 'demo',
      '#var' => $this->t('Onsen running on @name', ['@name' => \Drupal::config('system.site')->get('name')]),
    ];
    
  }

}
