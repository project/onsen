<?php

namespace Drupal\onsen\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;



/**
 * Configure example settings for this site.
 */
class OnsenSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'onsen.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'onsen_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    
    $form['onsen'] = [
      '#type' => 'details',
      '#title' => t('Onsen'),
      '#description' => t('Onsen settings'),
      '#open' => TRUE,
    ];

    $form['onsen']['themes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enabled themes'),
      '#default_value' => $config->get('themes'),
      '#description' => $this->t('Load Onsen for themes in list. A comma separated list as: bartik, zen, my_custom_theme'),
      '#attributes' => ['placeholder' => "enabled_theme, anotherone, yet_anotherone"],
    ];
    

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $config = $this->config("onsen.settings");
    
    $config
      ->set('themes', $form_state->getValue('themes'))
      ->save();

    parent::submitForm($form, $form_state);
    
  }

}
